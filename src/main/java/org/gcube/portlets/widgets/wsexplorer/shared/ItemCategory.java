/**
 * 
 */
package org.gcube.portlets.widgets.wsexplorer.shared;


/**
 * The Enum ItemCategory.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jun 30, 2015
 */
public enum ItemCategory {
	
	HOME,
	VRE_FOLDER;
}
