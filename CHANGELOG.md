
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.2.1] - 2024-11-18

- Fixing typo `org.gcube.common.storagehub.model.acls.ACL.getPricipal` [#28452]
- Ported to SHUB 2.X

## [v2.2.0] - 2021-03-26

### Bug fixes

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20991] Conflict with Bootstrap Modal fixed

### Enhancements

[#21045] Added a new behavior: startable on an input folder and 
being able to navigate from the Workspace Root


## [v2.1.2] - 2019-12-19

Updated to Git and Jenkins


## [v2.1.1] - 2019-09-10

Rebuilt due to storagehub exceptions added since gCube 4.14


## [v2.1.0] - 2019-08-02

Changed ItemBuilder to read the new method getMetadata


## [v2.0.1] - 2019-03-10

[Incident #77436] Fixing class cast Exception for FolderItem son of a SharedFolder

[Task #10943] Removed final to a Constant

Fixed breadcrumb on SelectDialaog


## [v2.0.0] - 2018-09-16

[Task #10943] Migrated to StorageHub


## [v1.8.0] - 2018-01-16

[Task #10943] Added breadcrumb changed event


## [v1.7.0] - 2017-07-06

[Feature #9114] Add pagination to Workspace Explorer


## [v1.6.1] - 2017-02-06

[Bug #6601] fixed


## [v1.6.0] - 2016-11-21

Removed ASL Dependency, ported to gCube Client Context


## [v1.5.0] - 2016-09-21

[Feature #5091] Added load for folder ID to SelectDialog and SelectPanel

Bug fixed on breadcrumb when item is null


## [v1.4.0] - 2016-05-31

[Feature #4128] Migration to Liferay 6.2


## [v1.3.0] - 2016-05-04

[Feature #2546] Endow Workspace Resources Explorer with filtering and display features


## [v1.2.1] - 2016-03-22

[Incident #2903] Bug fixed


## [v1.2.0] - 2016-02-26

Bug fix in Item.java

[Bug #2350] Bug fix


## [v1.1.1] - 2016-02-26

Bug fix in Item.java

[Bug #2350] Bug fix


## [v1.1.0] - 2016-02-02

[Feature #1657] Added Resource Explorer

Feature #2099] Add new facility: create a new folder


## [v1.0.0] - 2015-07-16

First Release


## [v0.0.1] - 2015-06-24

Started project
